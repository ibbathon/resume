from resume.models import Experience, Item, Link, Resume, Section


def run():
    Resume.objects.all().delete()
    r = Resume.objects.create(
        name="Richard Fears", nickname="Ibb", title="Software Engineer"
    )

    # Links
    Link.objects.create(
        resume=r, text="LinkedIn", url="https://www.linkedin.com/in/ibbathon/"
    )
    Link.objects.create(resume=r, text="GitHub", url="https://github.com/ibbathon")
    Link.objects.create(resume=r, text="GitLab", url="https://gitlab.com/ibbathon")

    # Work Experience
    s = Section.objects.create(
        resume=r, text="Work Experience", purpose="workexperience"
    )
    e = Experience.objects.create(
        section=s,
        text="NextEra Analytics",
        role="Software Engineer II",
        start_date="2022-12-12",
    )
    for order, text in enumerate(
        (
            "Lead developer on greenfield ETL plugin for translating internal "
            + "NE360 events to Python SOAP calls to the California Independent "
            + "System Operator (CAISO). Involved in planning and troubleshooting "
            + "sessions between NextEra and CAISO resources.",
            "Developed solutions involving Python, FastAPI, Flask, gRPC, Pants, "
            + "Docker, and AWS (Secrets, S3, EC2) and deployed them via Kubernetes "
            + "using kubectl, Helm, and ArgoCD. Solutions included ETL apps, "
            + "mathematical models, PaaS frameworks, and standalone scripts.",
            "Provided product support for internal and external customers using "
            + "the technologies above."
            "Participated in all standard scrum rituals (standup, grooming, "
            + "planning, retro, demo) as well as smaller engineering-focused "
            + "grooming sessions and larger scrum-of-scrum meetings across teams.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Vault Health",
        role="Senior Software Engineer",
        start_date="2021-07-12",
        end_date="2022-09-22",
    )
    for order, text in enumerate(
        (
            "Lead developer on integrations with insurance eligibility "
            + "providers and claim processors. Duties included acting as project "
            + "lead, writing technical specifications, performing story work, "
            + "coordinating deployment, and providing ongoing support for "
            + "product owners. During ongoing support, suggested and implemented "
            + "solutions to allow product owners to directly handle claim "
            + "troubleshooting and adjustments without engineer involvement, via "
            + "Retool integrations.",
            "Developer on new microservice application. Microservice was "
            + "written in Python FastAPI and TypeScript React. Coordinated "
            + "process and standards improvements across teams.",
            "Developer on a 3rd-party logistics client integration. "
            + "Integration was written in Python Flask.",
            "Wrote and presented technical specifications. Major tech specs "
            + "were on improving integrations with a 3rd-party logistics client, "
            + "to allow mass orders; and deprecation of tightly-integrated "
            + "denormalized data solution in favor of better-architected "
            + "normalized data solution.",
            "Mentored junior and mid-level engineers, focusing on helping the "
            + "engineers achieve their SMART goals and preparing them for the "
            + "challenges of mid-level expectations.",
            "Suggested and implemented improvements to the team’s scrum "
            + "rituals, including implementing and running sprint-end demos, "
            + "improving and running standups and retros, and coordinating and "
            + "running join sprint-end demos with another team.",
            "Championed better QA processes (proof-of-functionality, TestRail "
            + "integration) and coding standards improvements within and without "
            + "the team.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="LegalShield",
        role="Software Engineer",
        start_date="2019-10-21",
        end_date="2021-07-09",
    )
    for order, text in enumerate(
        (
            "Developed new lockbox implementation, to interface with Wells "
            + "Fargo’s automated lockbox deposits. ETL scripts written in "
            + "Python, communicating with Grooper for OCR and pattern recognition "
            + "and loading compiled data onto IBM i.",
            "Developed Python scripts to offload IBM i batch jobs from AS/400 "
            + "servers, as a continuing effort to transition from IBM i to the "
            + "new microservice architecture.",
            "Developed and wrote automated tests for microservices and "
            + "microsites as part of a company-wide rearchitecture initiative. "
            + "New backend stack written in C#.NET and React.",
            "Developed and wrote automated tests for the old backend "
            + "application. Backend stack used Ruby-on-Rails (as REST API) and "
            + "PostgreSQL. Automated tests used RSpec.",
            "Participated in and presented architecture discussions regarding "
            + "model design, interface changes, 3rd-party integration, and API "
            + "selection.",
            "Provided and received code reviews between and within teams, in "
            + "all languages listed above.",
            "Investigated parts of the SDLC process and implemented changes to "
            + "speed up automated tests, fix intermittently-failing tests, and "
            + "resolve data model inconsistencies.",
            "Acted as subject-matter expert for Postman and sections of the "
            + "backend API/model.",
            "Worked on multiple teams, including product development, "
            + "inter-team mediator, small-target initiatives, and technical "
            + "support.",
            "During technical support work, investigated and interpreted "
            + "backend and frontend code (Angular) to determine root causes and "
            + "implemented data fixes as needed.",
            "Participated in Scrum processes, including stand-ups, grooming, "
            + "and retrospectives.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="M-RETS",
        role="Consultant",
        start_date="2019-08-01",
        end_date="2019-09-18",
    )
    for order, text in enumerate(
        (
            "Developed and tested the application for Midwest Renewable Energy "
            + "Tracking System (M-RETS). Backend API written in Ruby-on-Rails "
            + "and frontend written in Ember. Automated testing was written in "
            + "RSpec, QUnit, and Cypress.",
            "Expanded and improved testing process, decreasing runtime of "
            + "integration tests by a third and changing the int tests to test "
            + "for relative changes instead of absolute text where relevant.",
            "Participated in Scrum processes, including stand-ups, grooming, "
            + "and retrospectives.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Vivial",
        role="Contractor",
        start_date="2019-03-11",
        end_date="2019-04-12",
    )
    for order, text in enumerate(
        (
            "Contract was for a Python expert to finish the last piece of an "
            + "SSO project for Vivial's Django-based (Python) Dashboard 360 "
            + "application. Contract was scheduled for 5-6 weeks, while most "
            + "code work was complete within 2 weeks.",
            "Implemented SSO connection from Dashboard 360 to an Auth0 "
            + "resource and several of Vivial's Node-based REST APIs.",
            "Implemented new menus and views in Dashboard 360 to communicate "
            + "with core service applications via REST API.",
            "Implemented automated unit tests for new Dashboard 360 functionality "
            + "and fixed existing broken tests.",
            "Modified core service applications to reduce load on core service "
            + "from Dashboard 360 account calls by batching calls.",
            "Dockerized seed scripts for Dashboard 360's Postgres and Mongo "
            + "databases so developers do not need to install dependencies on "
            + "local machines.",
            "Coordinated deployments and testing with QA team.",
            "Participated in Scrum processes, including stand-ups, grooming, "
            + "and retrospectives.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Zix",
        role="Software Developer",
        start_date="2018-04-02",
        end_date="2018-10-26",
    )
    for order, text in enumerate(
        (
            "Developed and maintained customer-facing web applications using "
            + "Ruby on Rails, Mithril, MySQL, Elasticsearch, and MongoDB",
            "Developed and maintained Perl driver script for Mimedefang Filter "
            + "instance",
            "Set up and maintained Amazon Web Services Content Distribution "
            + "Networks for web application, with separate buckets for "
            + "development, staging, and production environments",
            "Worked on a small team of developers/QAs with an Agile methodology "
            + "(informal Kanban)",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="WorkForce Software",
        role="Interfaces Engineer",
        start_date="2017-05-30",
        end_date="2018-02-27",
    )
    for order, text in enumerate(
        (
            "Developed JavaScript scripts to perform a variety of imports and "
            + "exports, including HR imports, payroll exports, general ledger "
            + "exports, vacation and sick leave imports and exports, worked time "
            + "imports, and Loose Data/Activity-Based Costing imports and "
            + "exports",
            "Formats included CSV flat files, SOAP-based web calls, and direct "
            + "database interaction",
            "Analysis and troubleshooting of customer and product issues, "
            + "including gathering interaction details, analysis of log files, "
            + "troubleshooting interface code, and deep dives into product "
            + "(Java) code",
            "Worked directly with customers to confirm requirements on both " + "sides",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Optum",
        role="Applications Developer",
        start_date="2013-04-15",
        end_date="2017-04-28",
    )
    for order, text in enumerate(
        (
            "Provided integration support for other company portal systems and "
            + "developers",
            "Wrote and ran unit tests, assembly tests, and system tests",
            "Communicated with and gathered requirements from internal "
            + "business customers",
            "Wrote functional and technical designs based on requirements from "
            + "internal business customers",
            "Developer and Solutions Analyst (SA) on PeopleSoft HCM and " + "LMS",
            "Primary developer and SA on Jive",
            "Collaborated on several projects to drive wider adoption of "
            + "internal Jive solution",
            "Monitored New Relic and Adobe Analytics statistics",
            "Development of Closure Template and HTML improvements, MS Report "
            + "Builder reports in SQL, and Python helper scripts to interface "
            + "with Jive’s REST API",
            "Coordinated testing for Jive's first-party modifications and " + "plugins",
            "Planned, wrote, and executed manual and automatic test scripts "
            + "for database ETL",
            "Designed, developed, and maintained a Java Swing application for "
            + "assisting in contract conversion",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Anoka Ramsey Community College",
        role="Adjunct Professor Mathematics",
        start_date="2011-08-01",
        end_date="2012-12-01",
    )

    e = Experience.objects.create(
        section=s,
        text="Michigan Technological University",
        role="Graduate Teaching Instructor Mathematics",
        start_date="2008-08-01",
        end_date="2011-06-01",
    )

    e = Experience.objects.create(
        section=s,
        text="MedNet Solutions Inc.",
        role="Intern",
        start_date="2005-01-01",
        date_text="Summers of 2005, 2006, 2007",
    )
    for order, text in enumerate(
        (
            "Clinical trials using online forms with data validation. Both QA "
            + "and developer duties, covering both testing and production "
            + "environments. Tcl, HTML, Javascript, and SQL queries developed "
            + "and used.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Anoka Ramsey Community College",
        role="Peer Tutor Math, Physics, and Computer Science",
        start_date="2003-01-01",
        end_date="2004-06-01",
    )

    # Skills
    s = Section.objects.create(resume=r, text="Technical Skills", purpose="skills")
    e = Experience.objects.create(
        section=s, text="Programming languages", start_date="1970-12-01"
    )
    for order, text in enumerate(
        (
            "Python",
            "Ruby",
            "C++",
            "JavaScript",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s, text="Major libraries", start_date="1970-11-01"
    )
    for order, text in enumerate(
        (
            "Elasticsearch",
            "Ruby on Rails",
            "React",
            "Django",
            "Flask",
            "FastAPI",
            "SolidJS",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s, text="Shell, design, and query languages", start_date="1970-10-01"
    )
    for order, text in enumerate(
        (
            "Bash",
            "HTML",
            "LaTeX",
            "MySQL",
            "MSSQL",
            "Oracle SQL",
            "PostgreSQL",
            "MongoDB",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s, text="Systems/Apps", start_date="1970-09-01"
    )
    for order, text in enumerate(
        (
            "Jira",
            "Git",
            "Microsoft Sharepoint",
            "Rancher",
            "Harness",
            "AWS",
            "Terraform",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s, text="Operating Systems", start_date="1970-08-01"
    )
    for order, text in enumerate(
        ("Microsoft Windows XP, 7, and 10", "Linux (large variety of distributions)")
    ):
        Item.objects.create(experience=e, text=text, order=order)

    # Education
    s = Section.objects.create(resume=r, text="Education", purpose="education")
    e = Experience.objects.create(
        section=s,
        text="Michigan Technological University",
        role="Houghton, MI",
        start_date="2004-08-01",
        end_date="2012-06-01",
    )
    for order, text in enumerate(
        (
            "M.S. in Mathematical Sciences, 2009, focus in numerical " + "analysis",
            "B.S. in Mathematical Sciences, 2007, Computer Science Minor",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)

    e = Experience.objects.create(
        section=s,
        text="Anoka-Ramsey Community College",
        role="Anoka, MN",
        start_date="2002-08-01",
        end_date="2004-06-01",
    )

    # Projects
    s = Section.objects.create(resume=r, text="Personal Projects", purpose="projects")
    e = Experience.objects.create(
        section=s,
        text="Publicly-available code",
        start_date="1970-12-01",
    )
    for order, data in enumerate(
        (
            (
                "Résumé rewrite (site/repo/playlist): A Django+SolidJS "
                + "SPA to replace the unwieldy RoR MPA below. MVP development "
                + "in-progress, with all coding being performed on live-stream and "
                + "following Agile SDLC practices.",
                {
                    "site": "https://ibbathon.com",
                    "repo": "https://gitlab.com/ibbathon/resume",
                    "playlist": (
                        "https://www.youtube.com/playlist?list="
                        + "PLXdi5VNjtDAx6HUTSqI2rbn56TUsSdJme"
                    ),
                },
            ),
            (
                "WaRT (Warframe Relic Tracker; site/repo): A user "
                + "inventory tracker for the online game Warframe. Built using "
                + "Flask, React, Node, and Mongo. Flask acts as a REST API to "
                + "deliver the data from Mongo to the React frontend. All parts of "
                + "the server have been dockerized for ease of deployment. Active "
                + "development with targeted release of July 2019.",
                {
                    "site": "https://wart.ibbathon.com",
                    "repo": "https://github.com/ibbathon/wart",
                },
            ),
            (
                "Old Résumé (site/repo): A Ruby-on-Rails application. "
                + "Built from the ground up to host my résumé at "
                + "ibbathon.com. The RoR code itself currently handles breaking the "
                + "résumé out into sections, experiences, items. It runs on a "
                + "PostgreSQL database. Active use since October 2018.",
                {
                    "site": "https://resume.ibbathon.com",
                    "repo": "https://github.com/ibbathon/wart",
                },
            ),
            (
                "YATTi (Yet Another Tracker of Time; repo): Python/Tkinter program to "
                + "track time on individual projects, written for professional "
                + "services environments. Tracking and analysis of feature progress "
                + "performed in a flat database. Active use since July 2017.",
                {"repo": "https://github.com/ibbathon/yatti"},
            ),
        )
    ):
        Item.objects.create(
            experience=e, text=data[0], keyword_links=data[1], order=order
        )

    e = Experience.objects.create(
        section=s, text="Hardware/Architecture projects", start_date="1970-11-01"
    )
    for order, text in enumerate(
        (
            "Raspberry Pi backup computers: Two RPi servers running Nextcloud, "
            + "acting as backup servers for me and my family. Periodically-run "
            + "script performs cross-site backup. My backup server also acts as "
            + "a private Git repository and media server. Active use since "
            + "2015.",
            "Timelapse camera: RPi server with an Arducam periodically taking "
            + "photos of my hydroponics. Currently in development.",
        )
    ):
        Item.objects.create(experience=e, text=text, order=order)
