import pytest

from resume.models import Resume


@pytest.fixture
def a_resume(db):
    return Resume.objects.create(name="Mr. Resume", nickname="Sir", title="Lord")


@pytest.fixture
def a_full_resume(
    db,
    a_resume,
    a_link,
    a_section,
    an_experience,
    a_second_experience,
    an_item,
):
    return a_resume
