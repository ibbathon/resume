from django.db import models

from resume.models import Link, Resume
from resume.models.base import UUIDModel
from tests.assertions import assert_field_basic_settings


class TestLinkModel:
    class TestHappyPath:
        def test_all_fields_match(self):
            correct_fields = sorted(
                [
                    "id",
                    "created_at",
                    "updated_at",
                    "resume_id",
                    "text",
                    "url",
                ]
            )
            model_fields = sorted(
                [
                    f.attname
                    for f in Link._meta.get_fields()
                    if type(f).__name__ not in ["ManyToOneRel", "ManyToManyRel"]
                ]
            )
            assert model_fields == correct_fields

        def test_inherits_from_base(self):
            assert Link.__base__ == UUIDModel

        def test_correct_field_settings(self):
            assert_field_basic_settings(
                Link,
                [
                    ["resume_id", models.ForeignKey, False, False],
                    ["text", models.CharField, False, False],
                    ["url", models.CharField, False, False],
                ],
            )
            assert Link.resume.field.related_model == Resume
            assert Link.text.field.max_length == 255
            assert Link.url.field.max_length == 255

        def test_str_returns_text(self):
            resume = Resume(name="aname", title="atitle")
            link = Link(resume=resume, text="atext", url="aurl")
            assert str(link) == "atext"
