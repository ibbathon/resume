import uuid
from unittest.mock import patch

import pytest
from django.db import models

from resume.models.base import UUIDModel


class TestUUIDModel:
    def setup_method(self, method):
        module = "resume.models.base."
        self.patchers = {
            "full_clean": patch(module + "UUIDModel.full_clean"),
            "super_save": patch(module + "models.Model.save"),
        }
        self.mocks = {k: v.start() for k, v in self.patchers.items()}

    def teardown_method(self, method):
        [p.stop() for p in self.patchers.values()]

    @pytest.fixture
    def mocks(self):
        return self.mocks

    class TestHappyPath:
        def test_all_fields_match(self):
            correct_fields = sorted(
                [
                    "id",
                    "created_at",
                    "updated_at",
                ]
            )
            model_fields = sorted(
                map(lambda f: f.attname, UUIDModel._meta.get_fields())
            )
            assert model_fields == correct_fields

        def test_correct_field_settings(self):
            assert isinstance(UUIDModel.id.field, models.UUIDField)
            assert UUIDModel.id.field.primary_key
            assert UUIDModel.id.field.default == uuid.uuid4
            assert not UUIDModel.id.field.editable

            assert isinstance(UUIDModel.created_at.field, models.DateTimeField)
            assert UUIDModel.created_at.field.auto_now_add

            assert isinstance(UUIDModel.updated_at.field, models.DateTimeField)
            assert UUIDModel.updated_at.field.auto_now

        def test_save_runs_full_clean(self, mocks):
            UUIDModel.save(UUIDModel, "test1", test2="test3")
            mocks["full_clean"].assert_called_once_with()

        def test_save_calls_super(self, mocks):
            UUIDModel.save(UUIDModel, "test1", test2="test3")
            mocks["super_save"].assert_called_once_with("test1", test2="test3")

        def test_is_abstract(self):
            assert UUIDModel._meta.abstract
