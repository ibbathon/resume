from django.db import models

from resume.models import Item
from resume.models.base import UUIDModel
from tests.assertions import assert_field_basic_settings


class TestItemModel:
    class TestHappyPath:
        def test_all_fields_match(self):
            correct_fields = sorted(
                [
                    "id",
                    "created_at",
                    "updated_at",
                    "experience_id",
                    "order",
                    "text",
                    "keyword_links",
                ]
            )
            model_fields = sorted(
                [
                    f.attname
                    for f in Item._meta.get_fields()
                    if type(f).__name__ not in ["ManyToOneRel", "ManyToManyRel"]
                ]
            )
            assert model_fields == correct_fields

        def test_inherits_from_base(self):
            assert Item.__base__ == UUIDModel

        def test_correct_field_settings(self):
            assert_field_basic_settings(
                Item,
                [
                    ["experience", models.ForeignKey, False, False],
                    ["order", models.IntegerField, False, False],
                    ["text", models.TextField, False, False],
                    ["keyword_links", models.JSONField, False, True],
                ],
            )
            assert Item.keyword_links.field.default == dict
            assert Item.order.field.default == 1

        def test_str_returns_name(self, an_experience):
            item = Item(experience=an_experience, text="atext")
            assert str(item) == "atext"

        def test_reorder_shifts_intermediate_numbers(self, an_experience):
            items = [
                Item(experience=an_experience, text="text1"),
                Item(experience=an_experience, text="text2"),
                Item(experience=an_experience, text="text3"),
            ]
            tuple(n.save() for n in items)

            assert tuple(n.order for n in items) == (1, 1, 1)
            Item.objects.reorder(items[2], 1)
            tuple(n.refresh_from_db() for n in items)
            assert tuple(n.order for n in items) == (2, 2, 1)
            Item.objects.reorder(items[0], 1)
            tuple(n.refresh_from_db() for n in items)
            assert tuple(n.order for n in items) == (1, 3, 2)
            Item.objects.reorder(items[0], 3)
            tuple(n.refresh_from_db() for n in items)
            assert tuple(n.order for n in items) == (3, 2, 1)
