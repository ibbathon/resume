def assert_field_basic_settings(
    model: type, fields: tuple[tuple[str, type, bool, bool]]
):
    """Accepts a Django model and a list of fields and their settings,
    then compares the field type, null, and blank to the requested settings.
    Example settings:
        ['name', models.CharField, False, False],
        ['nickname', models.CharField, False, True],
        ['title', models.CharField, False, False],
    (Resume.nickname.field is a CharField with null=False and blank=True)
    """
    for settings in fields:
        field = getattr(model, settings[0]).field
        assert isinstance(
            field, settings[1]
        ), f"type({settings[0]}) == {type(field)} != {settings[1]}"
        assert (
            field.null is settings[2]  # type: ignore
        ), f"{settings[0]}.null == {field.null} != {settings[2]}"  # type: ignore
        assert (
            field.blank is settings[3]  # type: ignore
        ), f"{settings[0]}.blank == {field.blank} != {settings[3]}"  # type: ignore
