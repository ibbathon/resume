from django.db import models, transaction

from resume.models.base import UUIDModel


class ItemManager(models.Manager):
    """Custom Item manager to allow reordering done at objects level"""

    def reorder(self, obj, new_order):
        qs = self.get_queryset()

        with transaction.atomic():
            if obj.order >= int(new_order):
                qs.filter(
                    experience=obj.experience,
                    order__lte=obj.order,
                    order__gte=new_order,
                ).exclude(pk=obj.pk).update(order=models.F("order") + 1)
            else:
                qs.filter(
                    experience=obj.experience,
                    order__gte=obj.order,
                    order__lte=new_order,
                ).exclude(pk=obj.pk).update(order=models.F("order") - 1)

            obj.order = new_order
            obj.save()


class Item(UUIDModel):
    experience = models.ForeignKey("Experience", on_delete=models.CASCADE)
    text = models.TextField(blank=False)
    keyword_links = models.JSONField(blank=True, default=dict)
    order = models.IntegerField(default=1)

    objects = ItemManager()

    def __str__(self):
        return str(self.text)

    class Meta:
        ordering = ["order"]
