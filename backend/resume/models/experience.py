from django.db import models

from resume.models.base import UUIDModel


class Experience(UUIDModel):
    section = models.ForeignKey("Section", on_delete=models.CASCADE)
    text = models.TextField(blank=False)
    role = models.TextField(blank=True)
    location = models.TextField(blank=True)
    start_date = models.DateField(null=False)
    end_date = models.DateField(null=True, blank=True)
    date_text = models.TextField(blank=True)

    def __str__(self):
        return str(self.text)

    class Meta:
        ordering = ["-start_date"]
