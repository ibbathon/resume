from django.db import models

from resume.models.base import UUIDModel


class Resume(UUIDModel):
    name = models.CharField(max_length=255, blank=False)
    nickname = models.CharField(max_length=255, blank=True)
    title = models.CharField(max_length=255, blank=False)

    def __str__(self):
        return str(self.name)
