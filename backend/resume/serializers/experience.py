from rest_framework import serializers

from resume.models import Experience
from resume.serializers.item import ItemSerializer


class ExperienceSerializer(serializers.ModelSerializer):
    items = ItemSerializer(many=True, read_only=True, source="item_set")

    class Meta:
        model = Experience
        fields = [
            "id",
            "text",
            "role",
            "location",
            "start_date",
            "end_date",
            "date_text",
            "items",
        ]
