from rest_framework import serializers

from resume.models import Resume
from resume.serializers.link import LinkSerializer
from resume.serializers.section import SectionSerializer


class ResumeSerializer(serializers.ModelSerializer):
    links = LinkSerializer(many=True, read_only=True, source="link_set")
    sections = SectionSerializer(many=True, read_only=True, source="section_set")

    class Meta:
        model = Resume
        fields = ["id", "name", "nickname", "title", "links", "sections"]
