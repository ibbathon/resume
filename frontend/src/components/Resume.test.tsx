import { Resume } from "./Resume";
import { render } from "solid-testing-library";
import { fetchDefaultResume } from "../actions";

jest.mock("../actions");
const mockedFetchDefaultResume = fetchDefaultResume as unknown as jest.Mock<
  typeof fetchDefaultResume
>;
mockedFetchDefaultResume.mockResolvedValue(
  // @ts-ignore: Mocks don't obey typing
  {
    // @ts-ignore: Mocks don't obey typing
    id: "345dec40-51d4-43f3-a8ed-08cf48cfbd84",
    name: "richard",
    nickname: "ibb",
    title: "cat",
    links: [
      {
        id: "fe758338-286e-40bd-8bf5-0029347ca207",
        text: "gitlab",
        url: "https://gitlab.com/ibbathon",
      },
    ],
    sections: [
      {
        id: "150d4127-1a65-4601-8ea7-42744f37f918",
        text: "a section",
        purpose: "a-section",
        experiences: [
          {
            id: "a518a122-a2ad-4023-9d2a-ab984cdbe4be",
            text: "an experience",
            role: "a role",
            location: "a location",
            start_date: "2022-09-22",
            end_date: "2022-09-30",
            date_text: "",
            items: [
              {
                id: "775108f2-1790-4e03-936c-d939f65ae9f4",
                text: "an item",
                order: 1,
                keyword_links: {
                  blah: "blahder",
                },
              },
            ],
          },
        ],
      },
    ],
  },
);

describe("Resume", () => {
  it("renders base resume profile info", async () => {
    const { findByText } = render(() => <Resume />);

    expect(await findByText("richard")).toBeInTheDocument();
    expect(await findByText("ibb")).toBeInTheDocument();
    expect(await findByText("cat")).toBeInTheDocument();
  });

  it("renders outside links", async () => {
    const { findByText } = render(() => <Resume />);

    const linkElement = (await findByText("gitlab")) as HTMLElement;
    expect(linkElement).toBeInTheDocument();
    expect(linkElement.tagName).toEqual("A");
    expect(linkElement.getAttribute("href")).toEqual(
      "https://gitlab.com/ibbathon",
    );
  });

  it("renders a section header, experience headers, and an item list item", async () => {
    const { findByText } = render(() => <Resume />);

    const sectionHeaderElement = (await findByText("a section")) as HTMLElement;
    const sectionSectionElement = sectionHeaderElement.parentElement;
    expect(sectionHeaderElement).toBeInTheDocument();
    expect(sectionSectionElement?.dataset.purpose).toEqual("a-section");

    const experienceElement = sectionHeaderElement.nextSibling?.childNodes[0];
    expect(experienceElement?.childNodes[0].childNodes).toHaveLength(3);
    expect(experienceElement?.childNodes[0].childNodes[0].textContent).toEqual(
      "an experience",
    );
    expect(experienceElement?.childNodes[0].childNodes[1].textContent).toEqual(
      "a role",
    );
    expect(experienceElement?.childNodes[0].childNodes[2].textContent).toEqual(
      "a location",
    );
    expect(experienceElement?.childNodes[1].childNodes).toHaveLength(2);
    expect(experienceElement?.childNodes[1].childNodes[0].textContent).toEqual(
      "2022-09-22",
    );
    expect(experienceElement?.childNodes[1].childNodes[1].textContent).toEqual(
      "2022-09-30",
    );

    const itemElement = experienceElement?.childNodes[2].childNodes[0];
    expect(itemElement?.textContent).toEqual("an item");
  });

  it("renders date_text instead of start_date if date_text is present", async () => {
    let data = await fetchDefaultResume();
    data.sections[0].experiences[0].date_text = "some date text";
    // @ts-ignore: Mocks don't obey typing
    mockedFetchDefaultResume.mockResolvedValue(data);

    const { findByText } = render(() => <Resume />);
    const sectionHeaderElement = (await findByText("a section")) as HTMLElement;
    const experienceElement = sectionHeaderElement.nextSibling?.childNodes[0];
    expect(experienceElement?.childNodes[1].childNodes[0].textContent).toEqual(
      "some date text",
    );
  });
});
