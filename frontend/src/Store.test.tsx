import { StoreProvider, useStore } from "./Store";
import { render } from "solid-testing-library";
import { For } from "solid-js";
import { Todo } from "./models/Todo";

const StoreTestingComponent = (props: { newTodos?: Todo[] }) => {
  const { todos, setTodos } = useStore();
  if (props?.newTodos) setTodos(props.newTodos);
  return (
    <>
      <For each={todos()}>
        {(todo, i) => {
          return (
            <>
              <p data-testid={`todo-text-${i()}`}>{todo.text}</p>
            </>
          );
        }}
      </For>
    </>
  );
};

describe("StoreContext", () => {
  beforeEach(() => {
    localStorage.removeItem("store");
  });

  const initialStore = {
    todos: [],
  };

  describe("Todo methods", () => {
    it("reads existing Todo data from localStorage", () => {
      const existingStore = {
        ...initialStore,
        todos: [{ id: 1, text: "existing todo", completed: false }],
      };
      localStorage.setItem("store", JSON.stringify(existingStore));
      const { getByTestId } = render(() => (
        <StoreProvider store={initialStore}>
          <StoreTestingComponent />
        </StoreProvider>
      ));
      expect(getByTestId("todo-text-0")).toHaveTextContent("existing todo");
    });

    it("updates store and localStorage on setTodos", () => {
      const newTodo = [{ id: 1, text: "new todo", completed: false }];
      const { getByTestId } = render(() => (
        <StoreProvider store={initialStore}>
          <StoreTestingComponent newTodos={newTodo} />
        </StoreProvider>
      ));
      expect(getByTestId("todo-text-0")).toHaveTextContent("new todo");
      expect(localStorage.store).toEqual(
        JSON.stringify({ ...initialStore, todos: newTodo }),
      );
    });
  });
});
