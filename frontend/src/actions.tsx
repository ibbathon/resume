import type { ResumeData } from "./models/Resume";

const fetchDefaultResume = async (): Promise<ResumeData> =>
  (await fetch(import.meta.env.VITE_API_URL + "/resumes/")).json();

export { fetchDefaultResume };
