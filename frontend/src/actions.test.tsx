import { fetchDefaultResume } from "./actions";

describe("fetchDefaultResume", () => {
  beforeEach(() => {
    // @ts-ignore This is due to fetchMock overriding with some mocks
    fetch.resetMocks();
  });
  it("calls localhost for dev runs and returns the JSON", async () => {
    // @ts-ignore This is due to fetchMock overriding with some mocks
    fetch.mockResponse(JSON.stringify({ thedata: "ishere" }));

    const result = await fetchDefaultResume();
    expect(result).toEqual({ thedata: "ishere" });
  });
});
