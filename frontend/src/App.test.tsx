import { render } from "solid-testing-library";
import App from "./App";
import { Resume } from "./components/Resume";

jest.mock("./components/Resume");
const mockedResume = Resume as unknown as jest.Mock<typeof Resume>;

describe("App", () => {
  beforeEach(() => {});

  it("renders Resume inside App", () => {
    render(() => <App />);
    expect(mockedResume.mock.calls).toEqual([[{}]]);
  });
});
