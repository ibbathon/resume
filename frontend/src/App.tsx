import { Component } from "solid-js";
import { Resume } from "./components/Resume";

const App: Component = () => {
  return <Resume />;
};

export default App;
